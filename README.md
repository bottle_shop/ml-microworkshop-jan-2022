# ML Microworkshop Jan 2022 

ML/DL Deep Learning Workshop - January 2022

## Organiser
- Prof. Roy Lee Ka-Wei
- Hee Ming Shan
- Hu Zhi Qiang
- Daniel Chin
- Md Rabiul Awal

## Schedule - 4th January (Day 1)

|  S/N  | Content                 | Speaker       |
| :---: | :---------------------- | ------------- |
|   1   | Opening & Project Brief | Roy           |
|   2   | Introduction to ML      | Daniel        |
|   3   | Break                   | -             |
|   4   | Bag-of-Words (TFIDF)    | Ming Shan     |
|   5   | Decision Tree           | Decision Tree |

## Schedule - 7th January (Day 2)

|  S/N  | Content        | Speaker   |
| :---: | :------------- | --------- |
|   1   | Word Sequence  | Zhi Qiang |
|   2   | Word Embedding | Zhi Qiang |
|   2   | TextCNNs       | Zhi Qiang |
|   3   | Break          | -         |
|   4   | RNN and LSTM   | Rabiul    |

## Video Recordings

The video recordings can be found via YouTube: https://youtube.com/playlist?list=PLKL7GnB7H50hyYxGadctmLQ11MMoAVJa2